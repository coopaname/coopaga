<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Quelques constantes utilisées dans ce thème
define("EQUIPE_PEDAGO_ID", 177);

if ( ! function_exists( 'neve_child_load_css' ) ):
	/**
	 * Load CSS file.
	 */
	function neve_child_load_css() {
		wp_enqueue_style( 'neve-child-style', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'neve-style' ), 'v1.1.0' );
	}
endif;
add_action( 'wp_enqueue_scripts', 'neve_child_load_css', 20 );


add_action( 'after_setup_theme', function () {
    load_child_theme_textdomain( 'coopaga', get_stylesheet_directory() . '/languages' );
});

/**
 * Modifie le readmore de NEVE pour afficher ce qu'on veut.
 */
add_filter('neve_read_more_args', function ($args) {

	$post_type = get_post_type();
	switch ($post_type) {
		case 'formation':
			$args['text'] = esc_html__( 'Voir la formation →', 'coopaga' );
			break;
		
		case 'session':
			// Est-ce qu'on peut s'inscrire
			$is_open = true;
			$post_id = get_the_id();
			$les_creneaux = get_post_meta($post_id, "creneaux", true);
			$today = new DateTime('now');
			foreach ($les_creneaux as $date => $value) {
				// Un des créneaux est passé
				if (date_create_from_format('j/m/Y', $date) < $today) {
					$is_open = false;
					continue;
				}
			}
			$args['text'] = $is_open ? esc_html__( "→ S'inscrire", 'coopaga' ) : esc_html__( "→ Voir", 'coopaga' );
			break;
	}

	$args['classes'] = 'readmore';

	return $args;
});

/** 
 * Modifie le titre pour les archives des contenus Formations 
 */
add_filter( 'get_the_archive_title', function ( $title ) {

	$post_type = get_post_type();
	switch ($post_type) {
		case 'formation':
			$title = __('Toutes nos formations', 'coopaga');
			break;
		
		case 'session':
			$title = __('Calendrier des sessions', 'coopaga');
			break;
	}

    return $title;

}, 20);

/** 
 * Met la classe comme pour les listes de posts pour les formateurs 
 */
add_filter( 'opaga_utilisateur_list_formateurs_classes', function ($classes) {
	return ['posts-wrapper'];
}, 20);

/**
 * Met la classe comme les listes de meta des posts pour les formateurs
 */
add_filter( 'opaga_utilisateur_formateur_tag_classes', function ($classes) {
	return ['nv-meta-list'];
});

/**
 * Met la pagination comme sur le reste du thème neve
 */
add_filter( 'opaga_utilisateur_pagination', function ($links) {
	$links = str_replace(
        array( '<a class="prev', '<a class="next' ),
        array(
            '<a rel="prev" class="prev',
            '<a rel="next" class="next',
        ),
        $links
    );
});

add_action( 'opaga_before_formateur', function () {
	echo '<div class="article-content-col"><div class="content"><div class="cover-post nv-post-thumbnail-wrap"><div class="inner">';
});

add_action( 'opaga_after_formateur', function () {
	echo '</div></div></div></div>';
});

/** 
 * On ne prend que les 3 premières formations dans la liste des formations 
 * quand on affiche la liste des formateurs.
 */
add_filter('opaga_utilisateur_formations', function ($formations) {
	return array_slice($formations, 0, 3);
});

/**
 * Affiche le nom de la formatrice dans le breadcrumb de la page qui affiche les infos formateur
 */
add_filter('wpseo_breadcrumb_links', function ($links) {
	if (is_page() && FORMATEUR_PAGE_ID == get_the_id()) {
		$user_id = um_profile_id();
		$formateur = new Formateur($user_id);
		$links = [
			$links[0], 
			['url' => get_the_permalink(EQUIPE_PEDAGO_ID), 'text' => get_the_title(EQUIPE_PEDAGO_ID), 'id' => EQUIPE_PEDAGO_ID], 
			['url' => $formateur->permalink, 'text' => $formateur->get_displayname(), 'id' => $user_id]
		];
		return $links;
	}

	return $links;
});

/**
 * Met le layout à covers pour le calendrier.
 */
add_filter( "theme_mod_neve_blog_archive_layout", function ($mod) {
	if ('session' == get_post_type()) {
		return 'covers';
	}
	return $mod;
});

/**
 * Supprime le READMORE worpdress pour les formations et sessions parce qu'on veut le mettre tout le temps. Pas que quand l'excerpt est tronqué.
 */
add_filter('excerpt_more', function ($more) {
	if (in_array(get_post_type(), ['session', 'formation'])) {
		return ' &hellip;';
	}
	return $more;
}, 20, 1);

/**
 * Ajoute le read more (qu'on a supprimé plus haut) dans tous les excerpts
 */
add_filter('the_excerpt', function ($content) {
	if (in_array(get_post_type(), ['session', 'formation'])) {
		$neve_view = new Neve\Views\Template_Parts();
		return $content . $neve_view->link_excerpt_more('');
	}

	return $content;
});

/**
 * Ne trime pas l'excerpt des sessions parce qu'il est court et qu'on a besoin du HTML pour le themer correctement.
 */
add_filter( 'wp_trim_excerpt', function ($text, $raw_excerpt) {

	if ('session' == get_post_type()) {
		return apply_filters( 'the_content', '' );
	}

	return $text;
}, 2, 20 );

/**
 * Affiche 12 posts par page.
 */
add_action( 'pre_get_posts', function ( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', 12 );
	}

  	return $query;
});

/*
add_filter( 'the_author_posts_link', function ( $link ) {
	global $post, $wpof;
	if( $post->post_type === 'session' || $post->post_type === 'formation') {
		$author_id      = $post->post_author;
		$formateurs = get_post_meta($post->ID, "formateur", true);
		$link = [];
		foreach ($formateurs as $form_id) {
			$user = get_userdata($form_id);
			$display_name = $user->display_name;
			$nicename = $user->user_nicename;
			$link[] = sprintf(
				'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
				esc_url(home_url()."/".$wpof->url_user."/".$user->user_login),
				esc_attr( sprintf( __( 'Voir le profil de %s', 'coopaga' ), $display_name ) ),
				$display_name
			);
		}
	}
	return implode(', ',  $link);
}, 10, 1 );
*/
//add_filter('filter_print_creneaux', 'tab_creneaux', 10, 2);
function single_list_creneaux($display, $creneaux)
{
	global $wpof;
        if (empty($creneaux))
		return $display;
        
        ob_start();
        ?>
        <?php foreach($creneaux as $date => $tab_creno) : ?>
	<p><strong><?php echo pretty_print_dates($date); ?></strong> : 
		<?php $creneaux_jour = array(); ?>
            <?php foreach($tab_creno as $c) : ?>
            <?php
                $creneaux_jour[] = $c->heure_debut." — ".$c->heure_fin;
            ?>
	    <?php endforeach; ?>
		<?php echo implode(' / ', $creneaux_jour); ?>
	</p>
	<?php endforeach; ?>
        <?php
	return ob_get_clean();
}
function tab_creneaux($display, $creneaux)
{
	global $wpof;
        if (empty($creneaux))
		return $display;
        
        ob_start();
        ?>
	<table>
        <?php foreach($creneaux as $date => $tab_creno) : ?>
	<tr><td><strong><?php echo pretty_print_dates($date); ?></strong></td>
		<?php $creneaux_jour = array(); ?>
            <?php foreach($tab_creno as $c) : ?>
		    <td><?php echo $c->heure_debut." — ".$c->heure_fin; ?></td>
	    <?php endforeach; ?>
	</tr>
		<?php endforeach; ?>
	</table>
        <?php
	return ob_get_clean();
}
